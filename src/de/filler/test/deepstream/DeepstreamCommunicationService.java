package de.filler.test.deepstream;

import io.deepstream.ConfigOptions;
import io.deepstream.ConnectionState;
import io.deepstream.ConnectionStateListener;
import io.deepstream.DeepstreamClient;
import io.deepstream.DeepstreamFactory;
import io.deepstream.LoginResult;

import java.util.Properties;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

class DeepstreamCommunicationService implements ConnectionStateListener {

	String									host;
	JsonObject								loginData;
	Gson									gson;
	static DeepstreamCommunicationService	instance;
	DeepstreamClient						client;
	boolean									startupComplete;
	boolean									reconnecting;

	private DeepstreamCommunicationService(final String deepstreamHost,
			final String deepstreamUser, final String deepStreamPassword)
			throws Exception {
		host = deepstreamHost;

		if (deepstreamUser != null && !deepstreamUser.equals("")) {
			loginData = new JsonObject();
			loginData.addProperty("username", deepstreamUser);
			loginData.addProperty("password", deepStreamPassword);
		} else {
			loginData = null;
		}

		gson = new Gson();

		try {
			connectOrReconnect();
		} catch (final Exception e) {
			System.out.println("Problem when connecting to deepstream: {}");
			throw e;
		}
	}

	public static DeepstreamCommunicationService start(
			final String deepstreamHost, final String deepstreamUser,
			final String deepStreamPassword) throws Exception {
		System.out.println("Starting service...");
		if (instance == null) {
			instance = new DeepstreamCommunicationService(deepstreamHost,
					deepstreamUser, deepStreamPassword);
		}
		System.out.println("Started.");
		return instance;
	}

	public void stop() throws Exception {
		System.out.println("Stopping service...");

		try {
			client.removeConnectionChangeListener(this);
			client.close();
			client = null;
		} catch (final Exception e) {
			System.out.println("Could not close deepstream connection: {}");
		}

		System.out.println("Stopped.");
	}

	private void connectOrReconnect() throws Exception {
		System.out.println("Connecting to deepstream...");

		final Properties properties = new Properties();
		properties.setProperty(ConfigOptions.MAX_RECONNECT_ATTEMPTS.toString(),
				"10");

		client = DeepstreamFactory.getInstance().getClient(host, properties);
		client.addConnectionChangeListener(this);

		LoginResult result = null;
		do {
			System.out.println("Trying to login...");

			if (loginData != null) {
				result = client.login(loginData);
			} else {
				result = client.login();
			}

			if (!result.loggedIn()) {
				System.out.println("Login and authentication failed.");

				try {
					Thread.sleep(1000);
				} catch (final InterruptedException e) {
					// Do nothing
				}
			}
		} while (startupComplete && !result.loggedIn());

		if (result.loggedIn()) {
			System.out.println("Login successful.");

			startupComplete = true;
			reconnecting = false;
			System.out.println("Connection to deepstream established.");

		} else {
			System.out
					.println("Could not login to deepstream server at startup: {}");
			throw new Exception(
					"Could not connect to deepstream server at startup!");
		}
	}

	@Override
	public void connectionStateChanged(final ConnectionState connectionState) {
		if (startupComplete && connectionState == ConnectionState.CLOSED) {
			if (reconnecting) {
				System.out.println("Deepstream connection still lost...");
			} else {
				System.out.println("Deepstream connection lost!");
				reconnecting = true;
			}

			try {
				connectOrReconnect();
			} catch (final Exception e) {
				System.out
						.println("Problem when reconnecting to deepstream deepstream: {}");
			}
		}
	}
}