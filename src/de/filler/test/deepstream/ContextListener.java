package de.filler.test.deepstream;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ContextListener implements ServletContextListener {
	private DeepstreamCommunicationService	deepstreamCommunicationService;

	@Override
	public void contextInitialized(final ServletContextEvent event) {
		try {
			deepstreamCommunicationService = DeepstreamCommunicationService
					.start(System.getProperty("ds_server"),
							System.getProperty("ds_user"),
							System.getProperty("ds_password"));
		} catch (final Exception e) {
			System.err.println(e.getLocalizedMessage());
			e.printStackTrace();
		}
	}

	@Override
	public void contextDestroyed(final ServletContextEvent event) {
		try {
			deepstreamCommunicationService.stop();
		} catch (final Exception e) {
			System.err.println(e.getLocalizedMessage());
			e.printStackTrace();
		}
	}

}
